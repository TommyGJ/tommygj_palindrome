require "tommygj_palindrome/version"

module TommygjPalindrome

  # Returns true for a palindrome, false otherwise.
	  def palindrome?
		  if processed_content.empty?
			  false
		  else
			  processed_content == processed_content.reverse
		  end
	  end

=begin	  def letters

		  the_letters = []
		  letter_regex = /[a-z]/i
		  self.chars.each do |character|
			  if character.match(letter_regex)
				  the_letters << character
		  	  end
		  end
		  the_letters.join
		  
		  self.chars.select { |c| c.match(/[a-z]/i) }.join
		  

		  the_letters = []
		  for i in 0..self.length - 1 do
			  if self[i].match(/[a-zA-z]/)
				  the_letters << self[i]
			  end
		  end
		  the_letters.join
	  end
=end
	  private

	  # Returns content for palindrome testing.
	  def processed_content
		  self.to_s.scan(/[a-zA-Z0-9]/i).join.downcase
	  end
end

class String
	include TommygjPalindrome
end

class Integer
	include TommygjPalindrome
end

